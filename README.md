# Jupyter Notebook

The [Jupyter](https://jupyter.org/) Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. Uses include: data cleaning and transformation, numerical simulation, statistical modeling, data visualization, machine learning, and much more.

## Usage via docker-compose.yml

```yaml
version: '2'

services:
    jupyter:
        image: swinkelhofer/jupyter-notebook:0.0.1-ml-kit
        volumes:
        - ./notebooks:/notebooks
        restart: always
        environment:
        - PASS=test
        ports:
        - 8080:80
```

## Environment variables

| Variable | Description | Default value |
|---------| ------------|----------------|
| PASS   | Password for the WebFrontend | secret |

## Tags

All tags are shipped with Python3. 

| Tag fragment | Description |
| ------| ----------|
| statistic-kit | with numpy, matplotlib, pandas and seaborn included |
| ml-kit | statistic-kit + tensorflow, keras and scikit-learn included |
| doc | pandoc and xetex included, to generate different outputs (pdf, html...) |
| r | R integration included |